set number
set tabstop=4
set shiftwidth=4
hi LineNr ctermfg=grey ctermbg=016
call plug#begin()
Plug 'preservim/NERDTree'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'https://github.com/ap/vim-css-color'
Plug 'https://github.com/bfrg/vim-cpp-modern'
call plug#end()
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_idx_mode = 1
map <F5> :NERDTreeToggle<CR>
map <F5> :NERDTreeToggle<CR>
nnoremap <C-Left> :bp<CR>
nnoremap <C-Right> :bn<CR>
inoremap ii <Esc>
